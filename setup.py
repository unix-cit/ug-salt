# coding=utf-8
from setuptools import setup

setup(
    name='ug_salt',
    version="0.0.5",
    author="Unixmaster",
    author_email="unixmaster@unige.ch",
    description="Tools to manage our salt",
    license = "gpl3 and above",

    # list all packages:
    # this meeans that ./foo/ug_salt.py will exists
    # eg:
    # packages = ["foo"],

    # list all the modules
    # this means that ./foo1.py and my_pkg/foo2.py must exists
    # eg:
    py_modules = [ "ug_salt" ],

    # list the libraries that are required
    # eg:
    #install_requires = ["libcbr"],

    # specify the entrypoints
    # "console_scripts" will create automatially script in /usr/local/bin
    # more info at:
    # https://amir.rachum.com/blog/2017/07/28/python-entry-points/
    # eg:
    # entry_points={
    #    'console_scripts': ['ug-lvmoracle = ug_lvmoracle:main'],
    #    'ch.unige.central_it.xymon.malert': ['estool = ug_estool:xymon']
    # }
    entry_points={
        'console_scripts': ['ug-salt = ug_salt:main'],
    }
)
