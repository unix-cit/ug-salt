#!/usr/bin/env python3
# Version 0.0.5
from __future__ import print_function
from pprint import pprint
import os
import sys
import argparse
import re
import salt.client
import socket
import subprocess
import yaml
import random

MINION_ID_DIR = "/srv/salt/pillar/minion_id"
TEMPLATE_DIR = "/srv/salt/pillar/minion_id/templates"

CLEAN_CMD_FMT=["salt-run fileserver.update",
               "salt '{}' saltutil.sync_all",
               "salt '{}' saltutil.refresh_pillar"]

UNIGE_YAML_PATH="/srv/salt/pillar/unige.sls"
# libcbr inline import

def get_FQDN_hostname():
    import socket
    return socket.gethostname()


def get_hostname():
    fqdnHostname = get_FQDN_hostname()
    return fqdnHostname.split('.')[0]


def ezcmd(cmd_str):
    proc = subprocess.Popen(cmd_str, stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True, cwd='/')
    lstdout = proc.stdout.readlines()
    lstderr = proc.stderr.readlines()
    proc.communicate()
    retcode = proc.wait()
    return (retcode, lstdout, lstderr)


def get_ip_4(host=None, port=0):
    # search for all addresses, but take only the v6 ones
    if not host:
        host=get_hostname()
    alladdr = socket.getaddrinfo(host, port)
    ip4 = filter(
        lambda x: x[0] == socket.AF_INET,  # means its ip6
        alladdr
    )
    # if you want just the sockaddr
    # return map(lambda x:x[4],ip6)
    return list(ip4)[0][4][0]


def get_ip_6(host=None, port=0):
    # search for all addresses, but take only the v6 ones
    if not host:
        host=get_hostname()
    alladdr = socket.getaddrinfo(host, port)
    ip6 = filter(
        lambda x: x[0] == socket.AF_INET6,  # means its ip6
        alladdr
    )
    # if you want just the sockaddr
    # return map(lambda x:x[4],ip6)
    return list(ip6)[0][4][0]

# end libcbr import

def minionname_list(pattern=""):
    rec = re.compile(pattern)
    retcode, lstdout, lstderr = ezcmd("salt-key -L")
    in_accepted_keys = False
    lname = []
    for line in lstdout:
        line = line.decode().strip()
        if "Accepted Keys:" == line:
            in_accepted_keys = True
            continue
        if line.find(" Keys") >= 0:
            break
        lname.append(line)
    lminionname = [name for name in lname if rec.search(name)]
    return lminionname

def clean(minion_id):
    for cmd_fmt in CLEAN_CMD_FMT:
        cmd=cmd_fmt.format(minion_id)
        print(cmd)
        retcode, lstdout, lstderr = ezcmd(cmd)
        print(" - retcode: ", retcode)
        print(" - lstout: ", "\n           ".join([l.rstrip() for l in lstdout]))
        print(" - lsterr: ", "\n           ".join([l.rstrip() for l in lstderr]))


def get_first_ip4_from_minion(minion_id):
    local = salt.client.LocalClient()
    lip4 = local.cmd(minion_id, "network.ipaddrs",)
    for ip4 in lip4[minion_id]:
        #print(ip4)
        return ip4
    return ""

def get_cidr4_prefix_from_minion(minion_id,ip4):
    local = salt.client.LocalClient()
    lsubnet = local.cmd(minion_id, "network.subnets", )
    for subnet in lsubnet[minion_id]:
        in_subnet=local.cmd(minion_id, "network.ip_in_subnet",[ip4,subnet])
        #pprint(in_subnet)
        if  in_subnet:
            return re.sub(r'^.*/([0-9]+)$',r'\1',subnet)
    return ""

def get_cidr6_prefix_from_minion(minion_id,ip6):
    local = salt.client.LocalClient()
    lsubnet = local.cmd(minion_id, "network.subnets6", )
    for subnet in lsubnet[minion_id]:
        in_subnet=local.cmd(minion_id, "network.ip_in_subnet",[ip6,subnet])
        #pprint(in_subnet)
        if in_subnet:
            return re.sub(r'^.*/([0-9]+)$',r'\1',subnet)
    return ""

def get_is_private_from_minion(minion_id,ip):
    local = salt.client.LocalClient()
    lis_private = local.cmd(minion_id, "network.is_private", [ip])
    return lis_private[minion_id]


def get_first_ip4_from_grains(minion_id):
    local = salt.client.LocalClient()
    grains = local.cmd(minion_id, "grains.items", )
    for interface_name, lip4 in grains[minion_id]["ip4_interfaces"].items():
        if "lo" == interface_name:
            continue
        for ip4 in lip4:
            return ip4
    return ""

def get_ip4_gw_from_grains(minion_id):
    local = salt.client.LocalClient()
    grains = local.cmd(minion_id, "grains.items", )
    lgw = grains[minion_id]["ip4_gw"]
    return lgw

def get_first_ip6_from_minion(minion_id,type):
    local = salt.client.LocalClient()
    lip6 = local.cmd(minion_id, "network.ipaddrs6", type=type)
    #pprint(lip6)
    for ip6 in lip6[minion_id]:
        if ip6[0:5]=="fe80":
            continue
        return ip6
    return ""

def get_ip6_gw_from_grains(minion_id):
    local = salt.client.LocalClient()
    grains = local.cmd(minion_id, "grains.items", )
    lgw = grains[minion_id]["ip6_gw"]
    return lgw

def get_first_ip6_from_grains(minion_id, prefix):
    local = salt.client.LocalClient()
    grains = local.cmd(minion_id, "grains.items", )
    for interface_name, lip6 in grains[minion_id]["ip6_interfaces"].items():
        if "lo" == interface_name:
            continue
        for ip6 in lip6:
            if ip6[0:5]=="fe80":
                continue
            return ip6
#            if ip6.lower().find(prefix.lower()) == 0:
#                if ip6[0:5]=="fe80":
#                    continue
#                return ip6
    return ""


def main_minion_list(pattern=""):
    lminionname = minionname_list(pattern=pattern)
    for name in lminionname:
        print(name)


def get_minion_id_path(minion_id):
    return os.path.join(MINION_ID_DIR, minion_id + ".sls")


def ask_which_apt_type():
    def ask_once():
        print("Which apt type is this machine Prod,Test,Cluster [t/P/c] :")
        apt_type=input()
        if not apt_type:
            apt_type="p"
        apt_type=apt_type[0].lower()
        return apt_type

    while True:
        apt_type=ask_once()
        if apt_type in ["t", "p", "c"]:
            return apt_type

def get_day():
    with open(UNIGE_YAML_PATH, "r") as stream:
        try:
            unige=yaml.load(stream)
        except:
            print("Unable to read the yaml file({})".format(UNIGE_YAML_PATH))
            sys.exit(1)
    rnd=random.randint(1,100)
    spread=[e.split(" ") for e in unige["unige"]["apt-spread"]]

    if 100 != sum([int(e[0]) for e in spread]):
        print("The sum of apt-spread defined in {} does not give 100 !".format(UNIGE_YAML_PATH))
        print("Exit !")
        sys.exit(1)

    cumulated_percentage=0
    for step_percentage, day in spread:
        cumulated_percentage += int(step_percentage)
        if rnd <= cumulated_percentage:
            return day

    print("We should no arrive here. It seems that there is a bug !")
    print("Cumalated_percentage = {}".format(cumulated_percentage))
    print("Exit.")
    sys.exit(1)

def main_minion_create(name, auto=False):
    ip4 = None
    ip6 = None
    minionid_path = get_minion_id_path(name)
    if name not in minionname_list():
        print("Unable to create minion_id/{} as this minion is not listed in 'salt-key -L' ! Exit.".format(name))
        sys.exit(1)
    if os.path.isfile(minionid_path):
        print("Minion_id ({}) already created ! Exit.".format(minionid_path))
        sys.exit(1)
    hostname = name + ".unige.ch"
    ip4=get_first_ip4_from_minion(name)
    #print("ip4="+ip4)
    if ip4==None:
        ip4 = input("Ip4 not found automagically, please provide the ip4 (empty if not needed): ")
        if ip4 == "":
            print("Exit.")
            sys.exit(1)
    #print("ip4="+ip4)
    if ip4:
        if get_is_private_from_minion(name,ip4):
            ip4_priv_or_pub = "private"
        else:
            ip4_priv_or_pub = "public"

    # ip6
    ip6 = get_first_ip6_from_minion(name,ip4_priv_or_pub)
    if not ip6:
        print("Ip6 not found automagically: provide the ip (empty if not needed)")
        ip6 = input("Do you want an ipv6 [y/n]? ")
        if not ip6:
            print("No ip6 given ! Exit.")
            sys.exit(1)
    #print("ip6="+ip6)
    if ip6:
        if get_is_private_from_minion(name,ip6):
            ip6_priv_or_pub = "private"
        else :
            ip6_priv_or_pub = "public"
        if "" == ip6_priv_or_pub:
            print("Ip6({}) does not belong to [2001|fd69]:620:600: !".format(ip6))
            print("Exit.")
            sys.exit(1)

    if ip4 and ip6:
        if ip4_priv_or_pub != ip6_priv_or_pub:
            print("Ip6({}) and Ip4({}) are one private and the other public !".format(ip6, ip4))
            print("Exit.")
            sys.exit(1)

    cidr4=ip4 + "/" + get_cidr4_prefix_from_minion(name,ip4)
    cidr6=ip6 + "/" + get_cidr6_prefix_from_minion(name,ip6)

    ip4_gw=get_ip4_gw_from_grains(name)
    ip6_gw=get_ip6_gw_from_grains(name)
    
    # write the template

    lnetwork_line = []
    network_template_path = os.path.join(TEMPLATE_DIR, "network")
    for line in open(network_template_path, "r").readlines():
        line = line.rstrip()
        newline = line.replace("{ipv4}", cidr4).replace("{ipv6}", cidr6).replace("{gateway4}",ip4_gw).replace("{gateway6}",ip6_gw)
        lnetwork_line.append(newline)

    #

    lapt_line=[]
    day=get_day()
    apt_type=ask_which_apt_type()
    apt_upgrade_type="apt-upgrade-{}".format({"p":"prod", "c":"cluster", "t":"test"}[apt_type])
    apt_template_path = os.path.join(TEMPLATE_DIR, apt_upgrade_type)
    for line in open(apt_template_path, "r").readlines():
        line=line.rstrip()
        newline=line.replace("{days}",day)
        lapt_line.append(newline)

    lminionid_line = lnetwork_line + lapt_line

    open(minionid_path, "w").writelines([l + os.linesep for l in lminionid_line])


def main():
    parser = argparse.ArgumentParser(description='tools to manage salt')
    subparsers = parser.add_subparsers(dest="sub", help="sub-command help")
    # utils
    utils_parser = subparsers.add_parser("utils", help="utils")
    utils_subparsers = utils_parser.add_subparsers(dest="subsub", help="utils-command")
    ## clean
    utilsclean_parser = utils_subparsers.add_parser("clean", help="clean for minion (minion_id)")
    utilsclean_parser.add_argument("minion_id", default="", type=str, help="minion_id")
    ## day
    utilsday_parser = utils_subparsers.add_parser("day", help="print a working-day-X using apt-spread defined in {}".format(UNIGE_YAML_PATH))
    # minion
    minion_parser = subparsers.add_parser("minion", help="minion")
    minion_subparsers = minion_parser.add_subparsers(dest="subsub", help="minion-command")
    ## list
    minionlist_parser = minion_subparsers.add_parser("list", help="show list (none:restarting, all:normal)")
    minionlist_parser.set_defaults(name="minion-list")
    minionlist_parser.add_argument("pattern", default="", type=str, help="re matching the indices", nargs='?', )
    ## create
    minioncreate_parser = minion_subparsers.add_parser("create", help="show create (none:restarting, all:normal)")
    minioncreate_parser.set_defaults(name="minion-create")
    minioncreate_parser.add_argument("name", type=str, help="re matching the indices")
    minioncreate_parser.add_argument("--auto", type=bool, help="do not ask question", default=False)

    args = parser.parse_args()

    did_something = False
    if "minion" == args.sub:
        if "create" == args.subsub:
            main_minion_create(args.name, auto=args.auto)
            did_something = True
        elif "list" == args.subsub:
            main_minion_list(pattern=args.pattern)
            did_something = True
    if "utils" == args.sub:
        if "clean" == args.subsub:
            clean(args.minion_id)
            did_something = True
        elif "day" == args.subsub:
            print(get_day())
            did_something = True

    if not did_something:
        parser.print_usage()


if __name__ == '__main__':
    main()
